package util

import (
	"fmt"
	"go/format"
	"os"
	"path/filepath"
	"strings"

	"golang.org/x/tools/imports"
)

// OpenOrCreate opens a file if it exists, otherwise creates it.
// If the file path contains directories, it will make them first.
func OpenOrCreate(file string) (*os.File, error) {
	if FileExists(file) {
		return os.OpenFile(file, os.O_RDWR|os.O_APPEND, 0644)
	}
	if i := strings.LastIndex(file, "/"); i != -1 {
		if err := os.MkdirAll(file[:i], 0755); err != nil {
			return nil, err
		}
	}
	return os.Create(file)
}

// Create Creates or truncates the named file,if the file path contains directories, it will make them first.
func Create(file string) (*os.File, error) {
	if !FileExists(file) {
		if i := strings.LastIndex(file, "/"); i != -1 {
			if err := os.MkdirAll(file[:i], 0755); err != nil {
				return nil, err
			}
		}
	}
	return os.Create(file)
}

// FileExists checks whether a file exists.
func FileExists(file string) bool {
	if _, err := os.Stat(file); os.IsNotExist(err) {
		return false
	}
	return true
}

// FormatGoCode 格式化go代码
func FormatGoCode(file string) error {
	c, err := os.ReadFile(file)
	if err != nil {
		return err
	}
	c, err = format.Source(c)
	if err != nil {
		return err
	}
	c, err = imports.Process(file, c, nil)
	if err != nil {
		return err
	}
	return os.WriteFile(file, c, 0666)
}

// 文件是否存在，不存递归创建文件夹
func FileExistsOrCreate(dir string) error {
	exi := FileExists(dir)
	if !exi {
		// 创建文件夹
		return os.MkdirAll(dir, os.ModePerm)
	}

	return nil
}

// GetDirs 获取文件夹下所有文件夹路径
func GetDirs(dirPath string) (dirs []string, err error) {
	err = filepath.Walk(dirPath, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			dirs = append(dirs, path)
		}
		return nil
	})
	if err != nil {
		fmt.Println("GetDirs Error :", err)
	}
	return
}

// GetProtoFiles 获取所有proto文件路径
func GetProtoFiles(dirPath string) ([]string, error) {
	dirs, err := GetDirs(dirPath)
	if err != nil {
		return nil, err
	}
	var files []string
	for _, dir := range dirs {
		fs, fErr := filepath.Glob(dir + "/*.proto")
		if fErr != nil {
			return nil, fErr
		}
		files = append(files, fs...)
	}

	return files, nil
}

// GetPbFiles 获取所有pb文件路径
func GetPbFiles(dirPath string) ([]string, error) {
	dirs, err := GetDirs(dirPath)
	if err != nil {
		return nil, err
	}
	var files []string
	for _, dir := range dirs {
		fs, fErr := filepath.Glob(dir + "/*.pb.go")
		if fErr != nil {
			return nil, fErr
		}
		files = append(files, fs...)
	}

	return files, nil
}

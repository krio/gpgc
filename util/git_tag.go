package util

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

type GiteeTagResp struct {
	Name    string `json:"name"`
	Message string `json:"message"`
}

func GetGiteeProjectTag(owner, repo string) (string, error) {
	repoURL := fmt.Sprintf("https://gitee.com/api/v5/repos/%s/%s/tags", owner, repo)
	// 发送 HTTP GET
	resp, err := http.Get(repoURL)
	if err != nil {
		return "", fmt.Errorf("请求失败：%+v", err)
	}
	defer resp.Body.Close()

	// 解析 JSON 响应
	//var result []*GiteeTagResp
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("请求解析响应失败：%+v", err)
	}
	// 数据绑定
	var datas []*GiteeTagResp
	err = json.Unmarshal(body, &datas)
	if err != nil {
		return "", fmt.Errorf("数据绑定：%+v", err)
	}

	return string(body), nil
}

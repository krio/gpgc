package util

import (
	"testing"
)

func TestGetModule(t *testing.T) {
	got, err := GetModuleName()
	if err != nil {
		t.Log(err)
		return
	}
	t.Log(got)
}

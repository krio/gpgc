package util

import (
	"testing"
)

func TestCreate(t *testing.T) {
	_, err := Create("test.go")
	t.Log(err)
}

func TestGetDirs(t *testing.T) {
	dirs, err := GetDirs("../example")
	if err != nil {
		t.Log(err)
		return
	}
	for _, dir := range dirs {
		t.Log(dir)
	}
}

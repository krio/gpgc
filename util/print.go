package util

import (
	"fmt"
	"os"

	"github.com/gookit/color"
)

func PrintEndFail(err string) {
	color.Redp(fmt.Sprintf("|* %s \n", err))
	os.Exit(1)
}

func PrintFail(err string) {
	color.Redp(fmt.Sprintf("|* %s \n", err))
}

func PrintFails(errList []string) {
	for _, err := range errList {
		PrintFail(err)
	}
}

func PrintError(err error) {
	color.Redp(fmt.Sprintf("|* error: %s \n", err.Error()))
	os.Exit(1)
}

func PrintSuccess(input string) {
	color.Greenp(fmt.Sprintf("|* success: %s \n", input))
}

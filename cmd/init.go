/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"gitee.com/krio/ginc/util"
	"github.com/spf13/cobra"
)

// initCmd represents the init command
var initCmd = &cobra.Command{
	Use:   "init",
	Short: "初始安装必要类库",
	Long:  `The man was too lazy to write it`,
	Run: func(cmd *cobra.Command, args []string) {
		// golang 1.17+
		_, err := util.GoInstallCmd("google.golang.org/protobuf/cmd/protoc-gen-go@v1.34.2")
		if err != nil {
			panic(err)
		}
		_, err = util.GoInstallCmd("google.golang.org/grpc/cmd/protoc-gen-go-grpc@1.4.0")
		if err != nil {
			panic(err)
		}
		_, err = util.GoInstallCmd("github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2@v2.20.0")
		if err != nil {
			panic(err)
		}
		_, err = util.GoInstallCmd("github.com/wangzhe1991/protoc-gen-go-errorx@latest")
		if err != nil {
			panic(err)
		}
		_, err = util.GoInstallCmd("github.com/favadi/protoc-go-inject-tag@v1.4.0")
		if err != nil {
			panic(err)
		}
	},
}

func init() {
	rootCmd.AddCommand(initCmd)
}

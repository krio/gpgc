package cmd

import (
	"github.com/spf13/cobra"

	"gitee.com/krio/ginc/gen"
	"gitee.com/krio/ginc/util"
)

// genCmd represents the gen command
var genCmd = &cobra.Command{
	Use:   "gen",
	Short: "根据proto创建api相关：pb、swagger、route、controller等",
	Long:  `根据proto创建api相关：pb、swagger、route、controller等`,
	Run: func(cmd *cobra.Command, args []string) {
		err := gen.Gender.Run()
		if err != nil {
			util.PrintError(err)
		}
		util.PrintSuccess("操作成功")
	},
}

func init() {
	rootCmd.AddCommand(genCmd)
	// 参数
	gen.Gender = &gen.Generator{}
	genCmd.Flags().StringVarP(&gen.Gender.ProtoPath, "proto", "p", "", "设置proto文件目标路径")
	genCmd.Flags().StringVarP(&gen.Gender.PbPath, "pb", "b", "", "设置生成pb文件路径")
	genCmd.Flags().StringVarP(&gen.Gender.SwaggerPath, "swagger", "w", "", "设置生成swagger文件路径")
	genCmd.Flags().StringArrayVarP(&gen.Gender.ThirdPartyPath, "third_party", "t", nil, "设置third party引用路径")
	genCmd.Flags().StringVarP(&gen.Gender.ControllerPath, "controller", "c", "", "设置生成controller文件路径")
	genCmd.Flags().StringVarP(&gen.Gender.Route, "router", "r", "", "设置路由文件")
}

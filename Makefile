.PHONY:lint
lint:
	golangci-lint run 

.PHONY:init
init:
	go run . init

.PHONY:version
version:
	go run . version

.PHONY:packr2
packr2:
	packr2 clean && packr2

.PHONY:client
client:
	go run . client -n demo -m demo

.PHONY: all
all:
	rm -rf example/swagger/*
	rm -rf example/controller/*
	rm -rf example/pb/*
	go run . gen -p example/proto \
		-b example/pb \
		-t example/third_party \
		-w example/swagger \
		-c example/controller \
		-r example/route


.PHONY: gitag
gitag:
	git add . && git commit -m "${tag}"
	git push
	git tag -a ${tag} -m "生成标签：${tag}" # 创建带标签的Tag
	git push origin ${tag}                # 推送Tag到远程




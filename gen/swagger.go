package gen

import (
	"fmt"
	"os"
	"regexp"
	"strings"

	"github.com/gookit/color"

	"gitee.com/krio/ginc/util"
)

// FormatSwaggerFile 编辑 api/swagger.json的info.description
func (g *Generator) FormatSwaggerFile() error {
	color.Bluep("[ 文档分组 ]", "\n")

	apiSwagger := g.SwaggerPath + "/apidocs.swagger.json"
	if !util.FileExists(apiSwagger) {
		return nil
	}
	bt, err := os.ReadFile(apiSwagger)
	if err != nil {
		return err
	}
	body := string(bt)
	serviceNodeMap := make(map[string]string, len(g.protoInfo))
	var maxNameLen int
	for _, v := range g.protoInfo {
		serviceNodeMap[v.Name] = v.NodeName
		length := len(v.Name)
		if length > maxNameLen {
			maxNameLen = length
		}
	}
	for svcName, nodeName := range serviceNodeMap {
		complexStr := fmt.Sprintf(`"%s"`, svcName)
		if strings.Contains(body, complexStr) {
			reg := regexp.MustCompile(complexStr)
			fs := reg.FindString(body)
			body = strings.ReplaceAll(body, fs, "\""+nodeName+"\"")
		}
		util.EqualLengthPrint(svcName, nodeName, maxNameLen)
	}
	if err = os.WriteFile(apiSwagger, []byte(body), 0644); err != nil {
		return err
	}
	return nil
}

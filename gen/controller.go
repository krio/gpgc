package gen

import (
	"bytes"
	"fmt"
	"os"
	"strings"
	"text/template"

	"gitee.com/krio/ginc/util"
)

var (
	Gender    *Generator
	headerStr = `package {{.PackageName}}

import (
	pb "{{.Import}}"

	"github.com/gin-gonic/gin"
)

`
	// 基于gin的func模板string
	funcStr = `// {{.Func.Name}} {{.Func.Desc}}
func {{.Func.Name}}(ctx *gin.Context, req *pb.{{.Func.Request}}) (*pb.{{.Func.Response}}, error) {
	return &pb.{{.Func.Response}}{}, nil // TODO
} `
)

// GenController 生产controller
func (g *Generator) GenController() error {
	defer func() {
		if err := recover(); err != nil {
			panic(err)
		}
	}()
	var (
		err error
		m   = make(map[string]string) // proto内容
	)
	for _, srv := range g.protoInfo {
		for _, api := range srv.APIs {
			var (
				exi            bool
				controllerStr  string
				controllerFile = fmt.Sprintf("%s/%s", g.ControllerPath, api.ControllerFile)
			)
			// 获取controller内容
			if controllerStr, exi = m[controllerFile]; !exi {
				if controllerStr, err = g.getController(controllerFile); err != nil {
					return err
				}
			}
			cs, err2 := g.handleController(srv, api, controllerStr)
			if err2 != nil {
				return err2
			}

			m[controllerFile] = cs
		}
	}
	if err = g.writeController(m); err != nil {
		return err
	}

	return nil
}

// 获取controller内容
func (g *Generator) getController(file string) (string, error) {
	if util.FileExists(file) {
		bt, err := os.ReadFile(file)
		if err != nil {
			return "", err
		}
		return string(bt), nil
	}
	return "", nil
}

// 处理template模板内容
func (g *Generator) handleTemplate(comment, tStr string, c *Controller) (string, error) {
	tmpl, err := template.New(controllerKey).Parse(tStr)
	if err != nil {
		return "", err
	}
	var buf bytes.Buffer
	if err = tmpl.Execute(&buf, c); err != nil {
		return "", err
	}

	return comment + "\n" + buf.String(), nil
}

func (g *Generator) handleController(srv *Service, api *API, comment string) (string, error) {
	importStr := g.tempPbPath + "/" + srv.PackageName
	if comment == "" {
		var err error
		comment, err = g.handleTemplate(comment, headerStr, &Controller{
			Module:      g.goModule,
			PackageName: srv.PackageName + "Controller",
			PbPath:      g.PbPath,
			Import:      importStr,
		})
		if err != nil {
			return "", err
		}
	} else {
		// 这里处理的是gin框架，其他另行修改
		if strings.Contains(comment, fmt.Sprintf("func %s(ctx *gin.Context", api.Name)) {
			return comment, nil
		}
	}

	return g.handleTemplate(comment, funcStr, &Controller{
		Import: importStr,
		Func: ControllerFunc{
			Name:     api.Name,
			Desc:     api.Desc,
			Request:  api.Request.Name,
			Response: api.Response.Name,
		},
	})
}

func (g *Generator) writeController(contents map[string]string) error {
	for file, content := range contents {
		f, err := util.Create(file)
		if err != nil {
			return err
		}
		_, err = f.WriteString(content)
		if err != nil {
			return err
		}
		err = f.Close()
		if err != nil {
			return err
		}
		if err = util.FormatGoCode(file); err != nil {
			fmt.Printf("failed to gofmt code %v, err=%v\n", file, err)
		}
	}
	return nil
}

// ControllerFunc controller函数信息
type ControllerFunc struct {
	Name     string // 函数名称
	Desc     string // 函数描述
	Request  string // 函数请求
	Response string // 函数返回
}

// Controller controller信息
type Controller struct {
	Module      string // 项目module
	PackageName string // 包名
	PbPath      string // pb文件路径
	Import      string // pb文件import
	Func        ControllerFunc
}

package gen

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"

	"gitee.com/krio/ginc/util"
	"github.com/gookit/color"
)

// Generator is responsible to init project and generates codes from proto.
type Generator struct {
	ProtoPath      string   // proto文件目录地址
	PbPath         string   // pb文件生成目录地址
	PbFiles        []string // pb文件目录地址
	ControllerPath string   // controller文件目录地址
	Route          string   // 路由文件生成目录地址
	ThirdPartyPath []string // 第三方文件目录地址
	SwaggerPath    string   // swagger文件生成地址

	tempPbPath string
	protoFiles []string
	goModule   string
	protoInfo  []*Service
	routeList  []*RouteInfo // 路由列表
}

func (g *Generator) Run() error {
	var err error
	// 获取所有的proto文件
	g.protoFiles, err = util.GetProtoFiles(g.ProtoPath)
	if err != nil {
		return fmt.Errorf("ginc gen failed: %+v", err)
	}
	if len(g.protoFiles) <= 0 {
		return fmt.Errorf("未发现proto相关文件: %s", g.ProtoPath)
	}
	// 获取项目的module
	g.goModule, err = util.GetModuleName()
	if err != nil {
		return err
	}
	g.tempPbPath = filepath.Join(g.goModule, g.PbPath)
	// 打印参数
	g.cmdPrint()
	// 生成pb、swagger等文件
	if err = g.cmd(); err != nil {
		return err
	}
	// 获取proto文件信息
	result := &Result{}
	g.protoInfo, result, err = ParseProto(g.protoFiles)
	if err != nil {
		return fmt.Errorf("ParseProto Error : %+v", err)
	}
	if len(result.ErrList) > 0 {
		util.PrintFails(result.ErrList)
		util.PrintEndFail("操作失败")
	}
	g.routeList = result.RouteList
	// 处理tag
	if err = g.HandleTagGen(); err != nil {
		return fmt.Errorf("HandleTagGen Error : %+v", err)
	}
	if g.ControllerPath != "" {
		// 生成controller模板文件
		if err = g.GenController(); err != nil {
			return fmt.Errorf("GenController Error : %+v", err)
		}
	}
	if g.Route != "" {
		if err = g.GenRoute(); err != nil {
			return fmt.Errorf("GenRoute Error : %+v", err)
		}
	}
	// 处理pb文件夹目录
	if err = g.HandlePbDir(); err != nil {
		return fmt.Errorf("HandlePbDir Error : %+v", err)
	}
	// 处理swagger
	if g.SwaggerPath != "" {
		if err = g.FormatSwaggerFile(); err != nil {
			return fmt.Errorf("FormatSwaggerFile Error : %+v", err)
		}
	}
	return nil
}

// 执行cmd命令：生成pb、swagger等
func (g *Generator) cmd() error {
	color.Bluep("[ 执行命令：protoc ]", "\n")
	if err := util.FileExistsOrCreate(g.PbPath); err != nil {
		return err
	}
	var args []string
	args = append(args, fmt.Sprintf("--proto_path=%s", g.ProtoPath)) // proto
	for _, thirdPartyPath := range g.ThirdPartyPath {
		args = append(args, fmt.Sprintf("--proto_path=%s", thirdPartyPath)) // third_party
	}
	args = append(args, "--go_out=.")
	if g.SwaggerPath != "" {
		if err := util.FileExistsOrCreate(g.SwaggerPath); err != nil {
			return err
		}
		args = append(args, fmt.Sprintf("--openapiv2_out=allow_merge=true:%s", g.SwaggerPath)) // swagger
	}
	// 获取所有proto文件名称
	for _, v := range args {
		color.Cyanp("|* ", v, "\n")
	}
	args = append(args, g.protoFiles...)
	cmd := exec.Command("protoc", args...)
	cmd.Stderr = os.Stderr
	_, err := cmd.Output()
	if err != nil {
		return err
	}

	return nil
}

func (g *Generator) cmdPrint() {
	// 打印配置参数
	color.Bluep("[ 配置列表 ]", "\n")
	color.Cyanp(fmt.Sprintf("|* proto:       %s \n", g.ProtoPath))
	color.Cyanp(fmt.Sprintf("|* pb:          %s \n", g.PbPath))
	color.Cyanp(fmt.Sprintf("|* swagger:     %s \n", g.SwaggerPath))
	color.Cyanp(fmt.Sprintf("|* third_party: %s \n", g.ThirdPartyPath))
	color.Cyanp(fmt.Sprintf("|* controller:  %s \n", g.ControllerPath))
	color.Cyanp(fmt.Sprintf("|* router:      %s \n", g.Route))
	color.Cyanp(fmt.Sprintf("|* proto文件数量:%d \n", len(g.protoFiles)))

}

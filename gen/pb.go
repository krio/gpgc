package gen

import (
	"os"
	"strings"
)

// HandlePbDir 处理pb文件夹
func (g *Generator) HandlePbDir() error {
	os.RemoveAll(g.PbPath)
	// 移动新生成的pb文件到pb目录下
	err := os.Rename(g.tempPbPath, g.PbPath)
	if err != nil {
		return err
	}
	os.RemoveAll(g.goModule)
	dirs := strings.SplitN(g.goModule, "/", 2)
	if len(dirs) > 0 {
		os.RemoveAll(dirs[0])
	}
	return nil
}

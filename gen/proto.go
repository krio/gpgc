package gen

import (
	"fmt"
	"os"
	"strings"

	"gitee.com/krio/ginc/util"
	"github.com/emicklei/proto"
	"github.com/gookit/color"
)

// Service proto定义的service
type Service struct {
	PackageName  string // 包名
	PackageAlias string // 包名别名
	Name         string // service名称
	File         string // proto文件url
	APIs         []*API // 接口
	NodeName     string // 注释名称
}

type Result struct {
	ErrList      []string
	ServiceCount int
	ApiCount     int
	RouteList    []*RouteInfo
}

type RouteInfo struct {
	ServiceName string // service名称
	ServiceDesc string // service描述
	FuncName    string // 函数名称
	FuncDesc    string // 函数描述
	Method      string // 请求方式
	URL         string // 路由地址
}

// API proto定义的api信息
type API struct {
	Name           string
	Desc           string
	Method         string
	URL            string
	ControllerFile string   // controller文件
	Request        *Message // 入参
	Response       *Message // 出参
}

// Message proto定义的message信息
type Message struct {
	Name   string
	File   string
	Fields []*Field
}

// Field message的字段信息
type Field struct {
	Name          string
	InlineComment string
}

// ParseProto 获取proto解析信息
func ParseProto(protoPaths []string) ([]*Service, *Result, error) {
	color.Bluep("[ 解析： proto ]", "\n")
	var (
		srvs   []*Service
		result = &Result{}
	)
	for _, p := range protoPaths {
		res, err := parseProto(p)
		if err != nil {
			return nil, nil, err
		}
		srvs = append(srvs, res...)
	}
	// msgs := make(map[string]*Message)
	// fill Request and Response of APIs
	// for _, srv := range srvs {
	// 	for _, api := range srv.APIs {
	// 		api.Request = msgs[api.Request.Name]
	// 		if api.Request == nil {
	// 			return nil, fmt.Errorf("not found definition `%v` of %v's request", api.Request.Name, api.Name)
	// 		}
	// 		api.Response = msgs[api.Response.Name]
	// 		if api.Response == nil {
	// 			return nil, fmt.Errorf("not found definition `%v` of %v's response", api.Response.Name, api.Name)
	// 		}
	// 	}
	// }
	counterMap := make(map[string]struct{})
	for _, v := range srvs {
		result.ServiceCount++
		for _, vv := range v.APIs {
			result.ApiCount++
			method := strings.ToUpper(vv.Method)
			Key := fmt.Sprintf("[%s]:%v", method, vv.URL)
			if _, exi := counterMap[Key]; exi {
				result.ErrList = append(result.ErrList, fmt.Sprintf("%s 重复定义", Key))
			}
			result.RouteList = append(result.RouteList, &RouteInfo{
				ServiceName: v.Name,
				ServiceDesc: v.NodeName,
				FuncName:    vv.Name,
				FuncDesc:    vv.Desc,
				Method:      method,
				URL:         vv.URL,
			})

			counterMap[Key] = struct{}{}
		}
	}

	color.Cyanp(fmt.Sprintf("|* service数量:%d \n", result.ServiceCount))
	color.Cyanp(fmt.Sprintf("|* api数量:%d \n", result.ApiCount))
	return srvs, result, nil
}

// 解析proto文件内容
func parseProto(file string) ([]*Service, error) {
	reader, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer reader.Close()

	parser := proto.NewParser(reader)
	res, err := parser.Parse()
	if err != nil {
		return nil, fmt.Errorf("new parser parse error : %+v", err)
	}

	var srvs []*Service
	for _, val := range res.Elements {
		switch vt := val.(type) {
		case *proto.Service: // 解析service
			srvs = append(srvs, parseService(vt, file))
		case *proto.Message: // 解析message
			//msgs[vt.Name] = parseMessage(vt, file)
		default:
			// pass
		}
	}
	return srvs, nil
}

type Target struct {
	PackageName string
	TargetFile  string
	NodeName    string
}

const (
	controllerKey = "controller"
	serviceKey    = "service"
)

// 获取要生成的目标 路径+文件名称 和 controller包名
func getTarget(ps *proto.Service) *Target {
	var (
		lineStr = ""
		data    = &Target{
			PackageName: controllerKey,
			TargetFile:  controllerKey + ".go",
			NodeName:    "未分组-" + ps.Name,
		}
	)
	if ps.Comment != nil {
		lineStr = ps.Comment.Lines[0]
	}
	// 提取数据
	lineStr = strings.TrimSpace(lineStr)
	serviceArgs := strings.Split(lineStr, "|")
	dataMap := make(map[string]string)
	for _, serviceArg := range serviceArgs {
		params := strings.Split(serviceArg, "=")
		if len(params) >= 2 {
			key := strings.TrimSpace(params[0])
			val := strings.TrimSpace(params[1])
			dataMap[key] = val
		}
	}

	// 提取注释中的go文件路径
	if val := dataMap[controllerKey]; val != "" {
		i := strings.Index(val, ".go")
		if i != -1 {
			data.TargetFile = val[0 : i+3]
			s := strings.Split(data.TargetFile, "/") // 这里获取controller的包名，如：v1/example.go => v1
			l := len(s)
			if l > 1 {
				data.PackageName = s[l-2]
			}
		}
	}
	// 注释名称
	if val := dataMap[serviceKey]; val != "" {
		data.NodeName = val
	}
	return data
}

// 解析service
func parseService(ps *proto.Service, file string) *Service {
	tar := getTarget(ps)
	s := &Service{
		PackageName:  tar.PackageName,
		PackageAlias: controllerKey + tar.PackageName,
		Name:         ps.Name,
		File:         file,
		NodeName:     tar.NodeName,
	}

	for _, e := range ps.Elements {
		switch v := e.(type) {
		// case *proto.Comment:
		case *proto.RPC:
			s.APIs = append(s.APIs, parseRPC(v, tar.TargetFile))
			// case *proto.Package:
		}
	}
	return s
}

// 解析RPC
func parseRPC(pr *proto.RPC, file string) *API {
	var desc string
	if pr.Comment != nil {
		desc = strings.TrimSpace(pr.Comment.Lines[0])
	}
	a := &API{
		Name:           pr.Name,
		Desc:           desc,
		ControllerFile: file,
		Request:        &Message{Name: pr.RequestType},
		Response:       &Message{Name: pr.ReturnsType},
	}

	for _, e := range pr.Elements {
		switch v := e.(type) {
		case *proto.Option:
			for _, vv := range v.Constant.OrderedMap {
				if vv.IsString && util.InStringArr([]string{"get", "post", "put", "patch", "delete"}, vv.Name) {
					a.Method = vv.Name
					a.URL = vv.Source
				}
			}
		}
	}
	return a
}

// func parseMessage(pm *proto.Message, file string) *Message {
// 	m := &Message{
// 		Name: pm.Name,
// 		File: file,
// 	}
// 	for _, e := range pm.Elements {
// 		switch v := e.(type) {
// 		case *proto.NormalField:
// 			field := &Field{Name: v.Name}
// 			if v.InlineComment != nil && len(v.InlineComment.Lines) > 0 {
// 				field.InlineComment = strings.TrimSpace(v.InlineComment.Lines[0])
// 			}
// 			m.Fields = append(m.Fields, field)
// 		}
// 	}
// 	return m
// }

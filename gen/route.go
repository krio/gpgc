package gen

import (
	"bytes"
	"fmt"
	"os"
	"text/template"

	"gitee.com/krio/ginc/util"
)

const (
	routePackageName = "route" // 包名称
)

// GenRoute 生成路由地址常量
// 结果案例如下：
// var ApiPingGET = RegApi("/v1/ping", "ping-Ping", "GET")
// var ExampleL1ExampleL1POST = RegApi("/v1/ping", "案例L1-案例L1", "POST")
// var ExampleL2ExampleL2POST = RegApi("/v1/ping", "案例L2-案例L2", "POST")
func (g *Generator) GenRoute() error {
	// 先删除目录下所有文件
	_ = os.RemoveAll(g.Route)
	err := os.MkdirAll(g.Route, os.ModePerm)
	if err != nil {
		return err
	}
	// 先生成必要的文件
	err = g.GenRouteBasics()
	if err != nil {
		return err
	}
	routeMap := make(map[string][]*RouteInfo)
	for _, v := range g.routeList {
		routeMap[v.ServiceName] = append(routeMap[v.ServiceName], v)
	}

	for serviceName, v := range routeMap {
		err = g.genOne(serviceName, v)
		if err != nil {
			return err
		}
	}

	return nil
}

func (g *Generator) genOne(serviceName string, routeList []*RouteInfo) error {
	genFilePath := fmt.Sprintf("%s/%s_gen.go", g.Route, serviceName)
	f, err := util.Create(genFilePath)
	if err != nil {
		return err
	}
	defer f.Close()
	var (
		contents       = fmt.Sprintf("package %s\n\n", routePackageName)
		roueList       = make([]string, 0, len(g.routeList))
		routeVarString = "var {{.ServiceName}}{{.FuncName}}{{.Method}} = RegApi(\"{{.URL}}\", \"{{.ServiceDesc}}-{{.FuncDesc}}\", \"{{.Method}}\") \n" // route定义string
	)
	tmpl := template.Must(template.New(routePackageName).Parse(routeVarString))
	for _, v := range routeList {
		var buf bytes.Buffer
		err = tmpl.Execute(&buf, v)
		if err != nil {
			return fmt.Errorf("template execute failed. err=%+v", err)
		}
		roueList = append(roueList, buf.String())
	}
	// 这里处理下排序
	util.SortStrings(roueList)
	for _, v := range roueList {
		contents += v
	}
	_, err = f.WriteString(contents)
	if err != nil {
		return err
	}
	if err = util.FormatGoCode(genFilePath); err != nil {
		return fmt.Errorf("gofmt code failed. err=%+v", err)
	}
	return nil
}

type RouteBasics struct {
	PackageName string `json:"package_name"` // 包名称
}

// GenRouteBasics 生成路由基本定义内容
// 检查和生成 route_basics_gen.go 文件
func (g *Generator) GenRouteBasics() error {
	content := `package {{.PackageName}}

// 此文件很重要！！！
// 切勿删除！！！

var apiRouteList []IApiRoute

type ApiRoute struct {
	URL         string   // 路由URL
	Name        string   // 路由名称
	Method      string   // 路由名称
	Middlewares []string // 路由中间件

	code string // 唯一标识符
}

// RegApi 注册api
func RegApi(url, name, method string) IApiRoute {
	if url == "" {
		panic("路由URL不能为空")
	}
	ar := ApiRoute{
		URL:         url,
		Name:        name,
		Method:      method,
		Middlewares: nil,
		code:        "",
	}
	apiRouteList = append(apiRouteList, ar)
	return ar
}

// GetApiRouteList 获取所有的api路由的数据列表
func GetApiRouteList() []IApiRoute {
	return apiRouteList
}

type IApiRoute interface {
	GetInfo() ApiRoute
	GetName() string
	GetURL() string
	GetCode() string
}

func (a ApiRoute) GetInfo() ApiRoute {
	return a
}

func (a ApiRoute) GetName() string {
	return a.Name
}

func (a ApiRoute) GetURL() string {
	return a.URL
}

func (a ApiRoute) GetCode() string {
	return a.code
}`
	data := &RouteBasics{
		PackageName: routePackageName,
	}
	tmpl := template.Must(template.New(routePackageName).Parse(content))
	var buf bytes.Buffer
	err := tmpl.Execute(&buf, data)
	if err != nil {
		return fmt.Errorf("template execute failed. err=%+v", err)
	}
	filePath := g.Route + "/api_basics_gen.go"
	f, err := util.Create(filePath)
	if err != nil {
		return err
	}
	defer f.Close()
	_, err = f.WriteString(buf.String())
	if err != nil {
		return err
	}
	if err = util.FormatGoCode(filePath); err != nil {
		return fmt.Errorf("gofmt code failed. err=%+v", err)
	}
	return nil
}

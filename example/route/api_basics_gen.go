package route

// 此文件很重要！！！
// 切勿删除！！！

var apiRouteList []IApiRoute

type ApiRoute struct {
	URL         string   // 路由URL
	Name        string   // 路由名称
	Method      string   // 路由名称
	Middlewares []string // 路由中间件

	code string // 唯一标识符
}

// RegApi 注册api
func RegApi(url, name, method string) IApiRoute {
	if url == "" {
		panic("路由URL不能为空")
	}
	ar := ApiRoute{
		URL:         url,
		Name:        name,
		Method:      method,
		Middlewares: nil,
		code:        "",
	}
	apiRouteList = append(apiRouteList, ar)
	return ar
}

// GetApiRouteList 获取所有的api路由的数据列表
func GetApiRouteList() []IApiRoute {
	return apiRouteList
}

type IApiRoute interface {
	GetInfo() ApiRoute
	GetName() string
	GetURL() string
	GetCode() string
}

func (a ApiRoute) GetInfo() ApiRoute {
	return a
}

func (a ApiRoute) GetName() string {
	return a.Name
}

func (a ApiRoute) GetURL() string {
	return a.URL
}

func (a ApiRoute) GetCode() string {
	return a.code
}
